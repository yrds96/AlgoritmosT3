#include "ItemPedido.h"
#include <iostream>

using std::cout;
using std::endl;

ItemPedido::ItemPedido(Livro* livro, int qnt){
    this->livro=livro;
    precoUnitario=livro->getPreco();
    quantidade=qnt;
}

void ItemPedido::setPreco(float precoUnitario){

this -> precoUnitario = precoUnitario;

}

void ItemPedido::setQuantidade(int quantidade){

this-> quantidade = quantidade;

}

float ItemPedido::getPreco(){

return precoUnitario;

}

int ItemPedido::getQuantidade(){


return quantidade;

}

void ItemPedido::mostra(){
    cout << livro->getTitulo() << " x" << getQuantidade() << " R$" << getPreco() << endl;
}
