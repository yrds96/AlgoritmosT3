#include "Livro.h"
#include <iostream>

using std::cout;
using std::endl;

Livro::Livro(string isbn, string titulo, string autor, float preco){
    this->isbn = isbn;
    this->titulo = titulo;
    this->autor = autor;
    this->preco = preco;

}

void Livro::setIsbn(string isbn){

this-> isbn = isbn;

}

void Livro::setTitulo(string titulo){

this-> titulo = titulo;

}

void Livro::setAutor(string autor){

this-> autor = autor;

}

void Livro::setPreco(float preco){

this-> preco = preco;

}

string Livro::getIsbn(){

return isbn;

}

string Livro::getTitulo(){

return titulo;

}

string Livro::getAutor(){

return autor;

}

float Livro::getPreco(){
    return preco;
}

void Livro::mostra(){
    cout << "ISBN: " << isbn << endl;
    cout << "Titulo: " << titulo << endl;
    cout << "Autor: " << autor << endl;
    cout << "Preco: " << preco << "\n" << endl;
}
