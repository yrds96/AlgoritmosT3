#include "Cliente.h"
#include <iostream>

using std::cout;
using std::endl;

int Cliente::proxCod=0;

Cliente::Cliente(string nome,string telefone, string email) {
    codigo=proxCod;
    proxCod++;
    this->nome = nome;
    this->telefone = telefone;
    this->email = email;

}

void Cliente::setNome(string nome) {


    this->nome = nome;

}

void Cliente::setTelefone(string telefone) {

    this->telefone = telefone;

}

void Cliente::setEmail(string email) {

    this->email = email;

}

int Cliente::getCodigo() {

    return codigo;

}

string Cliente::getNome() {

    return nome;

}

string Cliente::getTelefone() {

    return telefone;

}

string Cliente::getEmail() {

    return email;

}

Pedido* Cliente::addPedido(string data) {

    pedidos.push_back(Pedido(data));

    return &pedidos.back();

}

void Cliente::mostra() {
    cout << "Codigo: " << codigo << endl;
    cout << "Nome: " << nome << endl;
    cout << "Telefone: " << telefone << endl;
    cout << "eMail: " << email << "\n" << endl;

}

void Cliente::getPedidos(){
    for(unsigned int i = 0; i < pedidos.size(); i++)
        pedidos.at(i).mostra();
}
