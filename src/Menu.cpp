#include "Menu.h"
#include <fstream>

using std::ofstream;
using std::ifstream;



Menu::Menu(string titulo) {
    this->titulo=titulo;
}
void Menu::iniciar() {
    opcao = 0;

    while(opcao != 9) {
        cout << "-----!-----\n" <<
             titulo << "\n"
             "Selecione uma opção\n"
             "1. Cadastrar cliente\n"
             "2. Cadastrar livro\n"
             "3. Gravar livros\n"
             "4. Recuperar livros\n"
             "5. Fazer Pedido\n"
             "----Relatórios----\n"
             "6. Clientes\n"
             "7. Livros\n"
             "8. Pedidos por cliente\n"
             "9. Finalizar sistema\n"<<endl;

        cin >> opcao;
        switch(opcao) {
        case 1:
            cadastroCliente();
            break;
        case 2:
            cadastroLivro();
            break;
        case 3:
            gravarLivros();
            break;
        case 4:
            recuperarLivros();
            break;
        case 5:
            fazerPedido();
            break;
        case 6:
            relatorioClientes();
            break;
        case 7:
            relatorioLivros();
            break;
        case 8:
            relatorioPedidosCliente();
            break;
        case 9:
            cout << "Saindo do programa" << endl;
            cin.get();
            break;
        default:
            cout << "Opção inválida" << endl;
            cin.get();
            break;
        }

    }
}

void Menu::cadastroCliente() {

    string nome,telefone,email;

    cout<<" Insira o nome "<<endl;
    cin.ignore();
    getline(cin,nome);
    cout<<" Insira o telefone "<<endl;
    getline(cin,telefone);
    cout<<" Insira o email "<<endl;
    cin>>email;
    clientes.push_back(Cliente(nome,telefone,email));
    cout << "Cliente cadastrado com Sucesso!" << endl;

}

void Menu::cadastroLivro() {
    string isbn,titulo,autor;
    float preco;

    cout<<" Insira o isbn "<<endl;
    cin>>isbn;
    cout<<" Insira o titulo"<<endl;
    cin.ignore();
    getline(cin,titulo);
    cout<< " Insira o autor "<<endl;
    cin.ignore();
    getline(cin,autor);
    cout<<" Insira o preco " <<endl;
    cin>>preco;
    livros.push_back(new Livro(isbn,titulo,autor,preco));
}

void Menu::gravarLivros() {
    ofstream filout("LivrosGravados.txt");

    for(unsigned int i=0; i<livros.size(); i++) {
        filout<< livros.at(i)->Livro::getTitulo() << ";" ;
        filout<< livros.at(i)->Livro::getIsbn() << ";" ;
        filout<< livros.at(i)->Livro::getAutor() << ";" ;
        filout<< livros.at(i)->Livro::getPreco() <<endl;
    }

    filout.close();
}

void Menu::recuperarLivros() {
    livros.clear();
    ifstream arquivo("LivrosGravados.txt");
    string valor;

        Livro* livro = new Livro();

        getline(arquivo, valor, ';');
        livro->setTitulo(valor);
        getline(arquivo, valor, ';');
        livro->setIsbn(valor);






    livros.push_back(livro);
    delete livro;




}

void Menu::fazerPedido() {
    string isbn;
    int quantidade;
    bool finalizarPedido = false;

    unsigned int codigo;
    cout << "Insira o codigo do cliente" << endl;
    cin >> codigo;

    Cliente* cliente = getClienteByCodigo(codigo);
    Pedido* novoPedido = 0;
    if(cliente == 0) {
        cout << "Cliente não cadastrado!" << endl;
        finalizarPedido = true;
    } else {
        string data;
        cout << "Insira a data do pedido" << endl;
        cin >> data;
        novoPedido = cliente->addPedido(data);
    }

    while(!finalizarPedido) {
        Livro* livroNovo = 0;
        while(livroNovo == 0 && !finalizarPedido) {
            cout << "Insira o ISBN:" << endl;
            cin >> isbn;
            livroNovo = getLivroByIsbn(isbn);

            if(livroNovo == 0) {
                cout << "Livro não encontrado" << endl;
                cout << "Deseja finalizar a compra" << endl;

                char confirmacao;
                cin >> confirmacao;

                if(toupper(confirmacao) == 'S')
                    finalizarPedido = true;
            }
        }

        cout<<" Insira a quantidade: "<<endl;
        cin>>quantidade;

        novoPedido->addItem(livroNovo, quantidade);

        char confirmacao;
        cout << "Deseja adicionar mais outro item?"<<endl;
        cin >> confirmacao;

        if(toupper(confirmacao) == 'N')
            finalizarPedido = true;

    }
}

void Menu::relatorioClientes() {
    for(unsigned int i=0; i<clientes.size(); i++)
        clientes.at(i).mostra();
}

void Menu::relatorioLivros() {
    for(unsigned int i=0; i<livros.size(); i++)
        livros.at(i)->mostra();
}

void Menu::relatorioPedidosCliente() {
    unsigned int codigo;
    cin >> codigo;

    Cliente* cliente = getClienteByCodigo(codigo);
    if(cliente != 0) {
        cliente->getPedidos();
    } else {
        cout << "Cliente não cadastrado";
    }
}

Cliente* Menu::getClienteByCodigo(int codigo) {
    for(unsigned int i = 0; i < clientes.size(); i++)
        if(clientes.at(i).getCodigo() == codigo)
            return &clientes.at(i);

    return 0;
}

Livro* Menu::getLivroByIsbn(string isbn) {
    for(unsigned int i = 0; i< livros.size(); i++)
        if(livros.at(i)->getIsbn() == isbn)
            return livros.at(i);

    return 0;
}

