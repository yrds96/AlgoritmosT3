#include "Pedido.h"
#include <iostream>

using std::cout;
using std::endl;

int Pedido::proxNum=0;

Pedido::Pedido(string data) {

    numero=proxNum;
    proxNum++;
    this -> data = data;

}

void Pedido::addItem(Livro* livro, int qnt) {
    itens.push_back(ItemPedido(livro,qnt));
}

void Pedido::setData(string data) {

    this -> data = data;

}

string Pedido::getData() {

    return data;

}

int Pedido::getNumero() {

    return numero;

}

float Pedido::precoTotal() {

    float total=0.0;

    for(unsigned long i=0; i<itens.size(); i++) {

        total+= itens.at(i).getPreco()*itens.at(i).getQuantidade();

    }
    return total;
}

void Pedido::mostra(){
    cout << "Numero do pedido:"<< getNumero() << "  Data: " << getData() << endl;
    for(int i = 0; i < itens.size();i++)
        itens.at(i).mostra();

    cout << "Preço Total: " << precoTotal() << endl;
}

