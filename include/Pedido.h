#ifndef PEDIDO_H_INCLUDED
#define PEDIDO_H_INCLUDED

#include "ItemPedido.h"
#include "Livro.h"

#include <string>
#include <vector>

using std::vector;
using std::string;

class Pedido{
    static int proxNum;
    int numero;
    string data;
    vector<ItemPedido> itens;
    float precoTotal();

    public:
        Pedido(string=" ");
        int getNumero();
        void setData(string);
        string getData();
        void addItem(Livro*, int);
        void mostra();
};


#endif // PEDIDO_H_INCLUDED
