#ifndef CLIENTE_H_INCLUDED
#define CLIENTE_H_INCLUDED
#include <string>
#include <vector>

#include "Pedido.h"

using std::string;
using std::vector;



class Cliente{
    static int proxCod;
    int codigo;
    string nome, telefone, email;
    vector<Pedido> pedidos;

    public:
        Cliente(string=" ",string=" ",string=" ");
        int getCodigo();
        void setNome(string);
        string getNome();
        void setTelefone(string);
        string getTelefone();
        void setEmail(string);
        string getEmail();
        Pedido* addPedido(string);
        void mostra();
        void getPedidos();


};

#endif // CLIENTE_H_INCLUDED
