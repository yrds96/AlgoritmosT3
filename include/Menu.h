#ifndef MENU_H_INCLUDED
#define MENU_H_INCLUDED

#include "MenuItem.h"
#include "Cliente.h"
#

#include <vector>
#include <string>
#include <iostream>

using std::string;
using std::vector;
using std::cout;
using std::endl;
using std::cin;

class Menu{
    vector<Livro*> livros;
    vector<Cliente> clientes;
    unsigned int opcao;
    string titulo;

    Cliente* getClienteByCodigo(int);
    Livro* getLivroByIsbn(string);
    void cadastroCliente();
    void cadastroLivro();
    void gravarLivros();
    void recuperarLivros();
    void fazerPedido();

    void relatorioClientes();
    void relatorioLivros();
    void relatorioPedidosCliente();

public:
    Menu(string titulo="");

    void iniciar();
};

#endif // MENU_H_INCLUDED
