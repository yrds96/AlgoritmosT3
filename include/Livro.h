#ifndef LIVRO_H_INCLUDED
#define LIVRO_H_INCLUDED

#include <string>

using std::string;


class Livro{
    string isbn, titulo, autor;
    float preco;

    public:
        Livro(string="", string="", string="", float=0.0);
        void setIsbn(string);
        string getIsbn();
        void setTitulo(string);
        string getTitulo();
        void setAutor(string);
        string getAutor();
        void setPreco(float);
        float getPreco();
        void mostra();
};

#endif // LIVRO_H_INCLUDED
