#ifndef ITEMPEDIDO_H_INCLUDED
#define ITEMPEDIDO_H_INCLUDED

#include "Livro.h"

#include <vector>

using std::vector;

class ItemPedido{

    float precoUnitario;
    int quantidade;
    Livro* livro;

public:
    ItemPedido(Livro*, int);
    void setPreco(float);
    float getPreco();
    void setQuantidade(int);
    int getQuantidade();
    void mostra();

};


#endif // ITEMPEDIDO_H_INCLUDED
